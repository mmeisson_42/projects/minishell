# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2016/02/19 15:09:15 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = minishell

SRC_PATH = ./src/
SRC_NAME = ft_builtin_controller.c ft_cd.c ft_command_controller.c\
		   ft_command_parser.c ft_env.c ft_env_controller.c ft_env_lib1.c\
		   ft_env_lib2.c ft_error.c ft_exit.c ft_get_global_env.c\
		   ft_get_prompt.c ft_get_symbolic.c ft_minilib.c ft_minilib2.c\
		   ft_minilib_option.c ft_minilib_opts.c ft_print_prompt.c\
		   ft_setenv.c ft_signal.c ft_unsetenv.c ft_verbose_env.c minishell.c\
		   ft_exec_path.c

OBJ_PATH = ./obj/
OBJ_NAME = $(SRC_NAME:.c=.o) $(BLT_NAME:.c=.o)

INC_PATH = ./inc/

CFLAGS = -Wall -Werror -Wextra -g

LIB_PATH = ./libft/

RULE_LFT = all

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))
LIB = $(addprefix -L,$(LIB_PATH))

LIB_NAME = -lft
LDFLAGS = $(LIB) $(LIB_NAME)


all: $(NAME)

$(NAME): $(OBJ)
	make -C $(LIB_PATH) $(RULE_LFT)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	rm -rf $(OBJ_PATH)

fclean: clean
	rm -f $(NAME)

re: fclean all

reall: fclean
	make -C $(LIB_PATH) re
	make all

.PHONY: all clean fclean re
