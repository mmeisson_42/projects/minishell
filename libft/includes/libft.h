/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:20:40 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/02 14:17:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <string.h>
# include <stdlib.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_btree
{
	void			*content;
	size_t			content_size;
	struct s_btree	*right;
	struct s_btree	*left;
}					t_btree;

/*
** Index :
** Part I : Strings manipulation
** ** SubPart I : From libc.
** ** SubPart II : Non standard
** Part II : Memory's manipulation
** Part III : Number's manipulation
** Part IV : Data's test
** Part V : Printing
** Part VI : List's manipulation
** Part VII : Binary tree manipulation
*/

/*
** Part I: Strings manipulation
*/
/*
**  SubPart I: From libc.
*/
size_t				ft_strlen(const char *str);
char				*ft_strcpy(char *dest, const char *src);
char				*ft_strncpy(char *dest, const char *src, size_t n);
char				*ft_strdup(const char *s);
char				*ft_strcat(char *dest, const char *src);
char				*ft_strncat(char *dest, const char *src, size_t n);
size_t				ft_strlcat(char *dest, const char *src, size_t n);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strnstr(const char *haystack, const char *needle,
		size_t n);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strtolower(char *str);
/*
** Subpart II: Non standard
*/
char				*ft_strnew(size_t size);
char				*ft_strndup(const char *s, size_t n);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char*));
void				ft_striteri(char *s, void (*f)(unsigned int, char*));
char				*ft_strmap(const char *s, char (*f)(char));
char				*ft_strmapi(const char *s, char (*f)(unsigned int, char));
int					ft_strequ(const char *s1, const char *s2);
int					ft_strnequ(const char *s1, const char *s2, size_t n);
char				*ft_strsub(const char *s, unsigned int start, size_t len);
char				*ft_strjoin(const char *s1, const char *s2);
char				*ft_strnjoin(const char *s1, const char *s2, size_t n);
char				*ft_strover(char *s1, const char *s2);
char				*ft_strtrim(const char *s);
char				**ft_strsplit(const char *s, char c);

/*
** Part II : Memory's manipulation (Non standard)
*/
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
void				*ft_memset(void *s, int c, size_t n);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t n);
void				*ft_memchr(const void *d, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

/*
** Part III : Number's manipulation
*/
int					ft_atoi(const char *nptr);
char				*ft_itoa(int n);

/*
** Part IV : Data's tests
*/
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_tolower(int c);
int					ft_toupper(int c);

/*
** Part V : Printing
*/
void				ft_putchar(char c);
void				ft_putstr(const char *str);
void				ft_putendl(const char *str);
void				ft_putnbr(int n);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char *s, int fd);
void				ft_putendl_fd(char *s, int fd);
void				ft_putnbr_fd(int n, int fd);

/*
** Part VI : List's manipulation
*/
t_list				*ft_lstnew(const void *content, size_t content_size);
void				ft_lstdelone(t_list **alst, void (*del)(void*, size_t));
void				ft_lstdel(t_list **alst, void (*del)(void*, size_t));
void				ft_lstadd(t_list **alst, t_list *new);
void				ft_lstback(t_list **head, t_list *new);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list				*ft_lstsort(t_list *lst, int (*cmp)());

/*
** Following are additionnals functions
*/

/*
** Part VII : Binary Tree's manipulation
*/
t_btree				*ft_btreenew(const void *content, size_t content_size);
void				ft_btreeadd(t_btree **abtree, t_btree *new,
		int (*cmp)(const void*, const void*, size_t));
void				ft_btreedelone(t_btree **abtree,
		void (*del)(void*, size_t));
void				ft_btreedel(t_btree **abtree, void (*del)(void*, size_t));
t_btree				*ft_btreersrc(const t_btree *nbtree, const void *data,
		size_t data_size, int (*cmp)(const void*, const void*, size_t));
void				ft_btreeiter(t_btree *btree, void (*f)(t_btree *elem));
t_btree				*ft_btreemap(t_btree *btree, t_btree *(*f)(t_btree *elem));
size_t				ft_btreelevelcount(const t_btree *btree);
void				ft_btreeequ(t_btree **btree);

/*
** Part VIII : Sorting functions
*/
void				ft_swap(int *a, int *b);
void				ft_sortinsert(int *tab, size_t size);
#endif
