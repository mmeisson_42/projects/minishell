/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 11:56:54 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 15:57:41 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *s, size_t n)
{
	char *dest;

	if (!(dest = ft_memalloc(n)))
		return (NULL);
	ft_strncpy(dest, s, n);
	return (dest);
	return (NULL);
}
