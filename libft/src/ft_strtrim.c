/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:09:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/19 14:21:16 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(const char *s)
{
	char	*str;
	size_t	size;

	str = ft_strnew(ft_strlen(s) + 1);
	while (*s == ' ' || *s == '\t' || *s == '\n')
		s++;
	ft_strcpy(str, s);
	size = ft_strlen(str) - 1;
	while (str[size] == ' ' || str[size] == '\t' || str[size] == '\n')
		str[size--] = 0;
	return (str);
}
