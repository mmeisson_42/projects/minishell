/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreeiter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 14:20:52 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:05:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_btreeiter(t_btree *btree, void (*f)(t_btree *elem))
{
	if (btree)
	{
		if (btree->left)
			ft_btreeiter(btree->left, f);
		f(btree);
		if (btree->right)
			ft_btreeiter(btree->right, f);
	}
}
