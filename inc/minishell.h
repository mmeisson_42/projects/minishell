/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 18:06:43 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/29 13:58:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# define BUFF_SIZE 80

# include "libft.h"
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <signal.h>
# include <stdio.h>

# ifdef linux
#  include <sys/wait.h>
# endif

# define GREEN_COLOR "\e[32m"
# define DEFAULT_COLOR "\e[0m"

# define RET_SUCCESS EXIT_SUCCESS
# define RET_FAILURE EXIT_FAILURE

# define COMMON 1
# define SAFE 0

# define REWRITE 1
# define N_REWRITE 0

/*
** User-errors list
*/

typedef enum		e_user_error
{
	E_CHDIR,
	E_EXEC_DIR,
	M_ARGS,
	E_ARGS,
	E_USARG,
	W_COMMAND,
	E_ENOENT,
	E_NOTDIR,
	E_UNKNO,
}					t_user_error;

/*
** t_str is a list to have a pseudo "array of array" flexible
*/

typedef struct		s_id
{
	pid_t			curr_id;
	int				sig_state;
	int				last_exit;
}					t_id;

typedef struct		s_str
{
	struct s_str	*next;
	char			str[255];
}					t_str;

/*
** environment listing
*/

typedef struct		s_env
{
	struct s_env	*next;
	char			*var;
}					t_env;

/*
** function's list with name (CF builtins)
*/

typedef struct		s_fct
{
	char	name[20];
	int		(*fct)(int, char);
}					t_fct;

/*
** environment variable with associated letter
*/

typedef struct		s_var
{
	char		letter;
	char		name[20];
}					t_var;

/*
** linker between common and secured environment
*/

typedef struct		s_envs
{
	t_env			*common;
	t_env			*save;
}					t_envs;

/*
** Options stocking for env's builtin
*/

typedef struct		s_env_opts
{
	struct s_env_opts	*next;
	char				option;
	char				*arg;
}					t_env_opts;

/*
** Lists of argument's type given by the user
*/

typedef enum		e_type
{
	BUILTIN,
	EXEC,
	ARG,
	STOP,
	PIPE,
	REDIR
}					t_type;

void				ft_get_prompt(char **command);
char				**ft_command_parser(char **str, int *size);
void				ft_print_prompt(void);
int					ft_builtin_controller(int ac, char **av);
int					ft_command_controller(char **av, t_envs *envs);
void				ft_order_exec(int entries, char **argv);
char				*ft_varcpy(char *dst, char *src, size_t size);
int					ft_manage_execution(char *path, char **args, t_envs *envs);
void				ft_verbose_env(t_env_opts *opts, char **args);
void				ft_get_sym_path(char *pah);

int					ft_user_error(t_user_error err);
void				ft_internal_error(int code);
void				ft_set_arg_error(int *error);

t_env_opts			*ft_parse_options(char **str, char ***exec,
		t_envs **envs, int *error);
void				ft_load_option(t_env_opts **opts, char **str, int *error);
void				ft_load_empty_option(t_env_opts **opts, char o);
int					ft_load_args_option(t_env_opts **opts, char **options,
		char opt);
int					ft_is_option(t_env_opts *opts, char o);
t_env_opts			*ft_add_linked_option(char o, char *arg);
void				ft_deloptsall(t_env_opts **opts);
int					ft_p_option(t_env_opts *opts, char **av, t_envs *envs);
void				ft_u_option(t_env_opts *opts, t_envs *envs);

void				ft_strcpy_nozero(char *dst, char *src);
char				*ft_str_rejoin(char *dst, char *src);
char				*ft_strreplace(char **str1, char *str2);
size_t				ft_double_strlen(char *str1, char *str2);
int					ft_rewrite(char **var, char *value, size_t size,
		int rewrite);
int					ft_direct_path(char **command, t_envs *envs);
char				*ft_strdup_toc(char **src, char c);
char				*ft_strdup_tos(char **src, char *s);
int					in_array(char *str, char c);

t_env				*ft_envnew(char *key, char *value);
int					ft_envadd(char *key, char *value, int rewrite, t_env **env);
void				ft_delenv(t_env **env);
void				ft_delenvall(t_env **env);
void				ft_delenvs(t_envs **envs);
int					ft_del_path(char **path);

t_envs				*ft_load_sub_envs(t_env_opts *opts, t_envs *this_envs,
		char ***args);
t_envs				*ft_strs_to_envs(char **env);
char				*ft_get_global_env(char *var, int common);

t_env				*ft_envcpy(t_env *src);
t_envs				*ft_envscpy(t_envs *src);

int					ft_cd(int ac, char **av);
int					ft_env(int ac, char **av);
int					ft_setenv(int ac, char **av);
int					ft_unsetenv(int ac, char **av);
int					ft_exit(int ac, char **av);

void				ft_signal(int sig);

#endif
