/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prompt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 18:24:38 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 12:44:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_envs	*g_envs;

void		ft_print_prompt(void)
{
	char			curr_dir[1024];
	char			*tmp;
	size_t			size;

	ft_bzero(curr_dir, 1024);
	if ((tmp = ft_get_global_env("PWD", COMMON)))
		ft_strcpy(curr_dir, tmp);
	else
		getcwd(curr_dir, 1023);
	size = ft_strlen(curr_dir);
	if (size >= 30)
	{
		ft_memmove(curr_dir + 3, curr_dir + size - 25, 30);
		ft_strncpy(curr_dir, "...", 3);
		ft_strcpy(curr_dir + 28, "$> ");
	}
	else
	{
		ft_memmove(curr_dir + 3, curr_dir, sizeof(char) * size);
		ft_strncpy(curr_dir, "...", 3);
		ft_strcpy(curr_dir + 3 + size, "$> ");
	}
	ft_putstr(GREEN_COLOR);
	ft_putstr(curr_dir);
	ft_putstr(DEFAULT_COLOR);
}
