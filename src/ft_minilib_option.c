/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minilib_option.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:53:49 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 12:48:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern int		g_last_exit;

int				ft_p_option(t_env_opts *opts, char **av, t_envs *g_envs)
{
	struct stat		s_stat;
	char			*path;
	int				ret;

	while (opts)
	{
		if (opts->option == 'P' && opts->arg)
		{
			path = ft_str_rejoin(opts->arg, *av);
			if (stat(path, &s_stat) != -1)
			{
				ret = ft_manage_execution(path, av, g_envs);
				return (ret);
			}
			return (ft_user_error(E_ENOENT));
		}
		opts = opts->next;
	}
	return (EXIT_FAILURE);
}

void			ft_unset_builtin_env(char *var, t_env **env)
{
	size_t		size;
	t_env		*curr;
	t_env		*tmp;

	size = ft_strlen(var);
	while (*env && !ft_strncmp((*env)->var, var, size))
	{
		tmp = *env;
		*env = (*env)->next;
		free(tmp);
	}
	curr = *env;
	while (curr && curr->next)
	{
		if (!ft_strncmp(var, curr->next->var, size))
		{
			tmp = curr->next;
			curr->next = tmp->next;
			free(tmp);
		}
		curr = curr->next;
	}
}

void			ft_u_option(t_env_opts *opts, t_envs *g_envs)
{
	while (opts)
	{
		if (opts->option == 'u' && opts->arg)
		{
			ft_unset_builtin_env(opts->arg, &(g_envs->common));
		}
		opts = opts->next;
	}
}

void			ft_load_option(t_env_opts **opts, char **str, int *error)
{
	static char		empty_option[] = {"iv"};
	static char		args_option[] = {"Pu"};
	size_t			j;

	j = 1;
	if (ft_strchr(empty_option, (*str)[j]))
		ft_load_empty_option(opts, (*str)[j]);
	else if (ft_strchr(args_option, (*str)[j]))
	{
		if (!(str[1]))
			ft_set_arg_error(error);
		else
		{
			*error = 2;
			ft_load_args_option(opts, str, (*str)[j]);
		}
	}
	else
		ft_set_arg_error(error);
}

t_env_opts		*ft_parse_options(char **str, char ***exec,
		t_envs **envs, int *error)
{
	t_env_opts	*options;

	options = NULL;
	while (*(++str))
	{
		if (ft_strcmp(*str, "--") && (*str)[0] == '-')
		{
			ft_load_option(&options, str, error);
			if (*error == 2 && *(str++))
				*error = 0;
			else if (*error == 1)
				return (NULL);
		}
		else
		{
			if (!ft_strcmp(*str, "--"))
				str++;
			break ;
		}
	}
	*envs = ft_load_sub_envs(options, *envs, &str);
	*exec = str;
	return (options);
}
