/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env_controller.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 12:07:53 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 12:37:57 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env		*ft_split_environnement(char **env)
{
	t_env	*list;
	t_env	*head;

	head = NULL;
	if (*env)
	{
		if (!(list = ft_memalloc(sizeof(t_env))))
			ft_internal_error(0);
		if (!(list->var = ft_strdup(*env)))
			ft_internal_error(0);
		head = list;
		env++;
	}
	while (*env)
	{
		if (!(list->next = ft_memalloc(sizeof(t_env))))
			ft_internal_error(0);
		list = list->next;
		if (!(list->var = ft_strdup(*env)))
			ft_internal_error(0);
		env++;
	}
	return (head);
}

char		*ft_get_env(char *str, t_env *env)
{
	size_t		size;

	size = ft_strlen(str);
	while (env)
	{
		if (ft_strnequ(str, env->var, size))
			return ((env->var) + size + 1);
		env = env->next;
	}
	return (NULL);
}

t_envs		*ft_strs_to_envs(char **env)
{
	t_envs		*g_envs;
	char		*tmp;

	if (!(g_envs = ft_memalloc(sizeof(t_envs))))
		ft_internal_error(0);
	if (*env)
	{
		g_envs->common = ft_split_environnement(env);
	}
	if ((tmp = ft_get_env("PATH", g_envs->common)))
		g_envs->save = ft_envnew("PATH", tmp);
	else
	{
		g_envs->save = ft_envnew("PATH", "/usr/bin:/bin:/usr/sbin:/sbin:/usr/"
				"local/bin:/usr/local/munki");
	}
	return (g_envs);
}

int			ft_envadd(char *key, char *value, int rewrite, t_env **env)
{
	t_env		*tmp;
	size_t		size;

	size = ft_strlen(key);
	if (!*env)
		*env = ft_envnew(key, value);
	else
	{
		tmp = *env;
		if (!(ft_strncmp(key, tmp->var, size)))
			return (ft_rewrite((&tmp->var), value, size + 1, rewrite));
		while (tmp && tmp->next)
		{
			if (!(ft_strncmp(key, tmp->next->var, size)))
			{
				return (ft_rewrite((&tmp->next->var),
							value, size + 1, rewrite));
			}
			tmp = tmp->next;
		}
		tmp->next = ft_envnew(key, value);
	}
	return (EXIT_FAILURE);
}
