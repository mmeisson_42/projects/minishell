/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env_lib1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 12:33:23 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 10:42:29 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_delenv(t_env **env)
{
	if ((*env)->var)
		free((*env)->var);
	free(*env);
	*env = NULL;
}

t_env		*ft_envnew(char *key, char *value)
{
	t_env	*node;
	char	*str;
	size_t	size_k;
	size_t	size_v;

	size_k = ft_strlen(key);
	size_v = ft_strlen(value);
	if (!(node = ft_memalloc(sizeof(t_env))))
		ft_internal_error(0);
	if (!(str = ft_memalloc(sizeof(char) * (size_k + size_v + 2))))
		ft_internal_error(0);
	ft_strcpy(str, key);
	str[size_k++] = '=';
	ft_strcpy(str + size_k, value);
	node->var = str;
	return (node);
}

t_env		*ft_envcpy(t_env *src)
{
	t_env	*node;

	if (!src)
		return (NULL);
	if (!(node = ft_memalloc(sizeof(t_env))))
		ft_internal_error(0);
	if (!(node->var = ft_strdup(src->var)))
		ft_internal_error(0);
	node->next = ft_envcpy(src->next);
	return (node);
}

t_envs		*ft_envscpy(t_envs *src)
{
	t_envs	*tmp;

	tmp = NULL;
	if (src)
	{
		if (!(tmp = ft_memalloc(sizeof(t_envs))))
			ft_internal_error(0);
		tmp->common = ft_envcpy(src->common);
		tmp->save = ft_envcpy(src->save);
	}
	return (tmp);
}
