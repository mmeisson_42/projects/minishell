/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 10:52:57 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 10:37:09 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;
extern t_envs	*g_envs;

char	*ft_varcpy(char *dst, char *src, size_t size)
{
	size_t	size_dst;
	size_t	size_src;
	char	*str;

	size_dst = ft_strlen(dst);
	size_src = ft_strlen(src);
	if (size_src + size > size_dst)
	{
		if (!(str = ft_memalloc(sizeof(char) * (size_src + size + 1))))
			ft_internal_error(0);
		ft_strncpy(str, dst, size);
		ft_strcpy(str + size, src);
		return (str);
	}
	else
		ft_strcpy(dst + size, src);
	return (dst);
}

/*
** av[1] = key ; av[2] = value ; av[3] = (bool)rewrite? || NULL
*/

int		ft_setenv(int ac, char **av)
{
	int			rewrite;
	int			inc;

	if (ac < 3 || ac > 4)
	{
		g_id.last_exit = EXIT_FAILURE;
		return (ft_user_error(E_ARGS));
	}
	rewrite = (ac == 4) ? ft_atoi(av[3]) : 0;
	inc = (av[2][0] == '=') ? 1 : 0;
	ft_envadd(av[1], av[2] + inc, rewrite, &(g_envs->common));
	g_id.last_exit = EXIT_SUCCESS;
	return (EXIT_SUCCESS);
}
