/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/11 13:13:44 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/31 14:20:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;

int			ft_user_error(t_user_error code)
{
	static char		errors[9][50] = {
	"An error got encountered ... check your path",
	"An error got encountered ... check the name",
	"Too many arguments",
	"Bad number of arguments",
	"Bad arguments",
	"Command not found",
	"No such file or directory",
	"Not a directory",
	"Unknown error",
	};

	if (code < 9)
		ft_putendl(errors[code]);
	else
		ft_putendl(errors[8]);
	return (EXIT_FAILURE);
}

void		ft_internal_error(int code)
{
	static char		errors[1][30] = {
		"Your system is out of memory",
	};

	if (code == 0)
		ft_putendl(errors[0]);
	exit(EXIT_FAILURE);
}

void		ft_set_arg_error(int *error)
{
	ft_user_error(E_USARG);
	ft_putstr("env: usage: env [-iv] [-P utilPath] [-u name] "
			"[name=value] [utility[argument]]\n");
	g_id.last_exit = 1;
	*error = 1;
}
