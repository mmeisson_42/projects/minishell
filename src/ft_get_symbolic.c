/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_symbolic.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 13:28:13 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/11 09:58:45 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		ft_degrade_path(char *path)
{
	size_t		len;

	len = ft_strlen(path);
	if (path[--len] == '/')
		len--;
	while (len > 0 && path[len] != '/')
		len--;
	path[len] = '\0';
}

static void		ft_symmove(char *path, char *base_path, size_t i)
{
	size_t		len;

	len = ft_strlen(base_path);
	ft_memmove(path + len + 1, path + i, ft_strlen(path + i));
	if (path[i])
		path[len] = '/';
	ft_strcpy_nozero(path, base_path);
}

void			ft_get_sym_path(char *path)
{
	char	base_path[1024];
	size_t	i;

	i = 0;
	ft_bzero(base_path, sizeof(char) * 1024);
	strcpy(base_path, ft_get_global_env("OLDPWD", COMMON));
	while (path[i] == '.')
	{
		if (ft_strnequ(path + i, ".", 1) && !ft_strnequ(path + i, "..", 2))
			i += 2;
		else if (ft_strnequ(path + i, "..", 2))
		{
			i += ((path + i)[1] == '/') ? 3 : 2;
			ft_degrade_path(base_path);
		}
		else
			break ;
		while (path[i] == '/')
			i++;
	}
	ft_symmove(path, base_path, i);
}
