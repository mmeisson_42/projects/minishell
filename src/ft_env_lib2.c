/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tmp_env.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 15:32:23 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 11:02:37 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_delenvall(t_env **env)
{
	if (*env)
	{
		ft_delenvall(&((*env)->next));
		if ((*env)->var)
			free((*env)->var);
		free(*env);
		*env = NULL;
	}
}

void	ft_delenvs(t_envs **envs)
{
	t_envs	*tmp;

	if (*envs)
	{
		tmp = *envs;
		ft_delenvall(&tmp->common);
		ft_delenvall(&tmp->save);
		free(*envs);
		*envs = NULL;
	}
}
