/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_option.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/31 20:11:37 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/31 20:33:42 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "shell.c"

char		*ft_get_option(const char *options, const char **args, size_t argc)
{
	char		*opts;
	int			i;
	int			j;
	int			k;

	if (!(opts = ft_strnew(ft_strlen(options))))
		exit(0); // ft_memalloc_error();
	i = 1;
	k = 0;
	while (args[i] && args[i][0] == '-' && args[i][1] != '-')
	{
		j = 1;
		while (args[i][j])
		{
			if (!ft_strchr(options, args[i][j]))
			{
				ft_option_error(options, args[i][j]);
				free (opts);
				return (NULL);
			}
			else if (!ft_strchr(opts, args[i][j]))
				opts[k++] = args[i][j];
			j++;
		}
		i++;
	}
	return (opts);
}
