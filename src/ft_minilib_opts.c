/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minilib_opts.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:48:06 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/27 14:55:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env_opts			*ft_add_single_option(char o)
{
	t_env_opts		*node;

	if (!(node = ft_memalloc(sizeof(t_env_opts))))
		ft_internal_error(0);
	node->option = o;
	return (node);
}

int					ft_load_args_option(t_env_opts **opts, char **options,
		char opt)
{
	t_env_opts		*tmp;

	if (!options[1])
		return (EXIT_SUCCESS);
	if (!*opts)
		*opts = ft_add_linked_option(opt, options[1]);
	else
	{
		tmp = *opts;
		while (tmp->next)
		{
			if (tmp->option == opt)
			{
				if (opt == 'P')
					ft_strreplace(&(tmp->arg), options[1]);
				else if (!ft_strcmp(options[1], tmp->arg))
					return (EXIT_FAILURE);
			}
			return (EXIT_FAILURE);
			tmp = tmp->next;
		}
		tmp->next = ft_add_linked_option(opt, options[1]);
	}
	return (EXIT_FAILURE);
}

int					ft_is_option(t_env_opts *opts, char o)
{
	int		ret;

	ret = 0;
	while (opts)
	{
		if (opts->option == o)
			ret++;
		opts = opts->next;
	}
	return (ret);
}

t_env_opts			*ft_add_linked_option(char o, char *arg)
{
	t_env_opts		*node;

	if (!(node = ft_memalloc(sizeof(t_env_opts))))
		ft_internal_error(0);
	node->option = o;
	node->arg = ft_strdup(arg);
	return (node);
}

void				ft_load_empty_option(t_env_opts **opts, char o)
{
	t_env_opts		*tmp;

	if (!*opts)
		*opts = ft_add_single_option(o);
	else
	{
		tmp = *opts;
		while (tmp->next)
		{
			if (o != 'v' && tmp->option == o)
				return ;
			tmp = tmp->next;
		}
		if (o != 'v' && tmp->option == o)
			return ;
		tmp->next = ft_add_single_option(o);
	}
}
