/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 10:02:03 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/31 14:21:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;

int		ft_exit(int ac, char **av)
{
	int		exit_code;

	if (ac > 2)
		return (ft_user_error(M_ARGS));
	exit_code = (ac == 2) ? ft_atoi(av[1]) : 0;
	g_id.last_exit = exit_code;
	exit(exit_code);
}
