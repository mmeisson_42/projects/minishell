/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_global_env.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 09:56:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 12:36:49 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_envs		*g_envs;

char		*ft_get_global_env(char *str, int common)
{
	t_env		*env;
	size_t		size;

	if (common)
		env = g_envs->common;
	else
		env = g_envs->save;
	size = ft_strlen(str);
	while (env)
	{
		if (!ft_strncmp(str, env->var, size))
			return ((env->var) + size + 1);
		env = env->next;
	}
	return (NULL);
}
