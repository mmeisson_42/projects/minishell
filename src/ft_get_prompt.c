/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_prompt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 10:21:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/19 14:49:25 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;

static char		*ft_trimreplace(char **str)
{
	char	*tmp;

	if (*str)
	{
		if (!(tmp = ft_strtrim(*str)))
			ft_internal_error(0);
		ft_strdel(str);
		*str = tmp;
	}
	return (*str);
}

static char		*ft_manage_commands(char *term_stdin)
{
	static char		**commands = NULL;
	static size_t	i = 0;

	if (!term_stdin)
	{
		if (commands && commands[i])
			return (ft_trimreplace(&(commands[i++])));
		else
			return (NULL);
	}
	if (commands)
	{
		free(commands);
		commands = NULL;
	}
	i = 0;
	if (!(commands = ft_strsplit(term_stdin, ';')))
		ft_internal_error(0);
	if (commands[i])
	{
		ft_trimreplace(&(commands[i]));
		return (commands[i++]);
	}
	else
		return (NULL);
}

static int		ft_manage_command(char buffer[], char **tmp)
{
	char	*c;

	if (!(*tmp = ft_strover(*tmp, buffer)))
		ft_internal_error(0);
	if ((c = ft_strchr(*tmp, '\n')) ||
			(c = ft_strchr(*tmp, EOF)))
		*c = '\0';
	return (c) ? 1 : 0;
}

void			ft_get_prompt(char **command)
{
	int			ret;
	char		*tmp;
	char		buffer[BUFF_SIZE + 1];

	*command = ft_manage_commands(NULL);
	if (*command)
		return ;
	while (1)
	{
		tmp = NULL;
		ft_print_prompt();
		ft_bzero(buffer, BUFF_SIZE + 1);
		ret = read(0, buffer, BUFF_SIZE);
		if (ret > 1 && *buffer)
			if (ft_manage_command(buffer, &tmp) == 1)
				break ;
	}
	*command = ft_manage_commands(tmp);
}
