/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 14:52:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/24 09:12:16 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_envs	*g_envs;
extern t_id		g_id;

int		ft_empty_cd(void)
{
	char	**tmp;

	if (!(tmp = ft_memalloc(sizeof(char*) * 3)))
		ft_internal_error(0);
	tmp[1] = ft_get_global_env("HOME", COMMON);
	if (ft_cd(2, tmp) == EXIT_FAILURE)
	{
		tmp[1] = ft_get_global_env("HOME", SAFE);
		if (ft_cd(2, tmp) == EXIT_FAILURE)
		{
			free(tmp);
			return (ft_cd(1, NULL));
		}
		free(tmp);
		return (EXIT_SUCCESS);
	}
	free(tmp);
	return (EXIT_FAILURE);
}

int		ft_cd_error(char *dir)
{
	struct stat		s_stat;

	if (stat(dir, &s_stat) == -1)
		return (ft_user_error(E_ENOENT));
	else if (!S_ISDIR(s_stat.st_mode))
		return (ft_user_error(E_NOTDIR));
	else if (chdir(dir) == -1)
		return (ft_user_error(E_UNKNO));
	return (EXIT_SUCCESS);
}

int		ft_cd_parser(int ac, char **av, char *path)
{
	if (ac == 1)
		return (EXIT_SUCCESS);
	if (ac > 1 && ac < 4)
	{
		if (ft_strequ(av[1], "-L"))
		{
			if (ac == 3)
				ft_strcpy(path, av[2]);
			return (1);
		}
		if (ft_strequ(av[1], "-P"))
		{
			if (ac == 3)
				ft_strcpy(path, av[2]);
			return (2);
		}
		if (ac == 2)
		{
			ft_strcpy(path, av[1]);
			return (1);
		}
		if (ac == 3)
			return (-1);
	}
	return (-1);
}

void	ft_cd_special_cases(char *path)
{
	static t_var	array[3] = {
		{ '~', "HOME\0" },
		{ '\0', "HOME\0" },
		{ '-', "OLDPWD\0" },
	};
	char			*env;
	size_t			i;
	size_t			len;

	i = 0;
	while (i < 3)
	{
		if (array[i].letter == path[0])
		{
			env = ft_get_global_env(array[i].name, COMMON);
			if (env)
			{
				len = ft_strlen(env);
				ft_memmove(path + len - 1, path, sizeof(char) * len);
				ft_strcpy_nozero(path, env);
				return ;
			}
		}
		i++;
	}
}

int		ft_cd(int ac, char **av)
{
	char		path[1024];
	int			option;

	g_id.last_exit = EXIT_FAILURE;
	ft_bzero(path, 512);
	option = ft_cd_parser(ac, av, path);
	ft_cd_special_cases(path);
	if (ft_cd_error(path) == EXIT_FAILURE)
		return (EXIT_FAILURE);
	ft_envadd("OLDPWD", ft_get_global_env("PWD", COMMON),
			REWRITE, &(g_envs->common));
	if (option == 2)
	{
		ft_bzero(path, 512);
		getcwd(path, 512);
		ft_envadd("PWD", path, REWRITE, &(g_envs->common));
	}
	else
	{
		if (*path != '/')
			ft_get_sym_path(path);
		ft_envadd("PWD", path, REWRITE, &(g_envs->common));
	}
	g_id.last_exit = EXIT_SUCCESS;
	return (EXIT_SUCCESS);
}
