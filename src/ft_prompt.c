/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prompt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 18:24:38 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/29 13:49:39 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_print_prompt(void)
{
	static char		*curr_directory = NULL;
	char			*tmp;
	size_t			size;

	ft_strdel(&curr_directory);
	curr_directory = getcwd(curr_directory, 0);
	size = ft_strlen(curr_directory);
	if (size >= 30)
	{
		ft_strcpy(curr_directory + 3, curr_directory + size - 25);
		ft_strncpy(curr_directory, "...", 3);
		ft_strcpy(curr_directory + 28, "$> ");
	}
	else
	{
		if (!(tmp = ft_memalloc(sizeof(char) * (size + 7))))
			ft_internal_error(0);
		ft_strcpy(tmp, "...");
		ft_strcpy(tmp + 3, curr_directory);
		ft_strcpy(tmp + 3 + size, "$> ");
		free(curr_directory);
		curr_directory = tmp;
	}
	ft_putstr(curr_directory);
}
