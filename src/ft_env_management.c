/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env_management.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/13 12:33:23 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/27 14:53:57 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*ft_envcpy(t_env *src)
{
	t_env	*node;

	if (!src)
		return (NULL);
	if (!(node = ft_memalloc(sizeof(t_env))))
		ft_internal_error(0);
	if (!(node->var = ft_strdup(src->var)))
		ft_internal_error(0);
	node->next = ft_envcpy(src->next);
	return (node);
}

t_envs	*ft_envscpy(t_envs *src)
{
	t_envs	*tmp;

	tmp = NULL;
	if (src)
	{
		if (!(tmp = ft_memalloc(sizeof(t_envs))))
			ft_internal_error(0);
		tmp->common = ft_envcpy(src->common);
		tmp->save = ft_envcpy(src->save);
	}
	return (tmp);
}

void	ft_delenvall(t_env **env)
{
	if (*env)
	{
		ft_delenvall(&((*env)->next));
		if ((*env)->var)
			free((*env)->var);
		free(*env);
		*env = NULL;
	}
}

void	ft_delenvs(t_envs **envs)
{
	t_envs	*tmp;

	if (*envs)
	{
		tmp = *envs;
		ft_delenvall(&tmp->common);
		ft_delenvall(&tmp->save);
		free(*envs);
		*envs = NULL;
	}
}
