/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 10:27:37 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 10:37:09 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;
extern t_envs	*g_envs;

static int		ft_delnextenv(t_env *env)
{
	t_env		*tmp;

	tmp = env->next;
	env->next = tmp->next;
	ft_delenv(&tmp);
	g_id.last_exit = EXIT_SUCCESS;
	return (EXIT_SUCCESS);
}

int				ft_unsetenv(int ac, char **av)
{
	t_env		*env;
	size_t		size;
	char		*var;

	if (ac != 2)
		return (ft_user_error(E_ARGS));
	var = av[1];
	size = ft_strlen(var);
	env = g_envs->common;
	if (env && !ft_strncmp(var, env->var, size))
	{
		g_envs->common = env->next;
		ft_delenv(&env);
		g_id.last_exit = EXIT_SUCCESS;
		return (EXIT_SUCCESS);
	}
	while (env && env->next)
	{
		if (!ft_strncmp(var, env->next->var, size))
			return (ft_delnextenv(env));
		env = env->next;
	}
	g_id.last_exit = EXIT_FAILURE;
	return (EXIT_FAILURE);
}
