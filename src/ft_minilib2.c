/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minilib2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 12:46:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/19 15:14:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void			ft_strcpy_nozero(char *dst, char *src)
{
	while (*src)
		*(dst++) = *(src++);
}

void			ft_deloptsall(t_env_opts **opts)
{
	if (*opts)
	{
		ft_deloptsall(&((*opts)->next));
		if ((*opts)->arg)
			free((*opts)->arg);
		free(*opts);
		*opts = NULL;
	}
}

int				ft_del_path(char **path)
{
	free(path);
	return (EXIT_SUCCESS);
}

char			*ft_strdup_toc(char **src, char c)
{
	char	*tmp;
	char	*tmp2;
	size_t	size;

	tmp = *src;
	while (*tmp && *tmp != c)
		tmp++;
	size = tmp - *src;
	tmp2 = tmp;
	while (*tmp2 == c)
		tmp2++;
	if (!(tmp = ft_strnew(size)))
		ft_internal_error(0);
	ft_strncpy(tmp, *src, size);
	*src = tmp2;
	return (tmp);
}

char			*ft_strdup_tos(char **src, char *s)
{
	char	*tmp;
	char	*tmp2;
	size_t	size;

	tmp = *src;
	while (*tmp && !in_array(s, *tmp))
		tmp++;
	size = tmp - *src;
	tmp2 = tmp;
	while (in_array(s, *tmp2))
		tmp2++;
	if (!(tmp = ft_strnew(size)))
		ft_internal_error(0);
	ft_strncpy(tmp, *src, size);
	*src = tmp2;
	return (tmp);
}
