/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec_command.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/07 13:51:03 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/19 15:09:25 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern int		g_last_exit;

char				**get_env_path(t_envs *envs, int common)
{
	size_t		i;
	t_env		*env;

	i = 0;
	if (common)
		env = envs->common;
	else
		env = envs->save;
	while (env)
	{
		if (!ft_strncmp(env->var, "PATH=", 5))
			return (ft_strsplit(env->var + 5, ':'));
		env = env->next;
	}
	return (NULL);
}

static int			ft_manage_exec_path(char **s_path, char **command,
		t_envs *envs)
{
	char			*path;
	int				i;

	i = 0;
	if (!(path = ft_str_rejoin(*s_path, *command)))
		ft_internal_error(0);
	if (ft_exec_path(path, command, envs) == EXIT_SUCCESS)
	{
		while (s_path[i])
			free(s_path[i++]);
		return (EXIT_SUCCESS);
	}
	free(*s_path);
	return (EXIT_FAILURE);
}

int					ft_del_path(char **path)
{
	free(path);
	return (EXIT_SUCCESS);
}

int					ft_exec_command(char **command, t_envs *envs)
{
	char			**s_path;
	int				i;
	int				ft_bool;

	i = ft_direct_path(command, envs);
	if (i != 2)
		return (EXIT_SUCCESS);
	else
	{
		i = 0;
		ft_bool = 1;
		while (ft_bool-- >= 0)
			if ((s_path = get_env_path(envs, ft_bool)) != NULL)
			{
				while (s_path[i])
					if (ft_manage_exec_path(s_path + i++, command, envs) ==
							EXIT_SUCCESS)
						return (ft_del_path(s_path));
				ft_del_path(s_path);
			}
	}
	g_last_exit = EXIT_FAILURE;
	ft_user_error(W_COMMAND);
	return (EXIT_FAILURE);
}
