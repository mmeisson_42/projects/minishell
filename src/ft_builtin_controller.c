/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_builtin_controller.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/07 14:40:41 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 10:33:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;

int		ft_builtin_controller(int ac, char **av)
{
	static t_fct	tab[5] = {
		{ "cd", ft_cd },
		{ "env", ft_env },
		{ "setenv", ft_setenv },
		{ "unsetenv", ft_unsetenv },
		{ "exit", ft_exit },
	};
	size_t			i;

	i = 0;
	while (i < 5)
	{
		if (!ft_strcmp(av[0], tab[i].name))
		{
			g_id.last_exit = tab[i].fct(ac, av);
			return (EXIT_SUCCESS);
		}
		i++;
	}
	return (EXIT_FAILURE);
}
