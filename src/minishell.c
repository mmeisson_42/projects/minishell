/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/06 17:25:15 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/19 14:21:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_id		g_id = { 1, 0, 0 };
t_envs		*g_envs = NULL;

void		ft_order_exec(int entries, char **argv)
{
	if (ft_builtin_controller(entries, argv) != EXIT_SUCCESS)
		ft_command_controller(argv, g_envs);
}

void		ft_update_pwd(void)
{
	char		**tab;
	char		*tmp;
	size_t		i;

	if (!(ft_get_global_env("PWD", 1)))
	{
		if (!(tab = malloc(sizeof(char*) * 5)) ||
				!(tab[1] = ft_strdup("PWD")) ||
				!(tab[2] = ft_memalloc(sizeof(char) * 512)))
			ft_internal_error(0);
		getcwd(tab[2], 511);
		tab[3] = ft_strdup("1");
		tab[4] = NULL;
		ft_setenv(4, tab);
		free(tab[1]);
		if (!(tab[1] = ft_strdup("OLDPWD")))
			ft_internal_error(0);
		if (!(tmp = ft_get_global_env("OLDPWD", COMMON)))
			ft_setenv(4, tab);
		i = 1;
		while (i < 4)
			free(tab[i++]);
		free(tab);
	}
}

void		ft_manage_execs(char **command)
{
	char	**argv;
	size_t	i;
	int		entries;

	entries = 0;
	if ((argv = ft_command_parser(command, &entries)))
	{
		i = 0;
		ft_order_exec(entries, argv);
		while (argv[i])
			free(argv[i++]);
		free(argv[i]);
		free(argv);
	}
	ft_strdel(command);
}

void		ft_manage_lvl(void)
{
	char	*tmp;
	int		level;

	tmp = ft_get_global_env("MNSLVL", COMMON);
	if (tmp)
		level = ft_atoi(tmp) + 1;
	else
		level = 1;
	ft_envadd("MNSLVL", ft_itoa(level), REWRITE, &(g_envs->common));
}

int			main(int ac, char **av, char **env)
{
	char		*command;

	command = NULL;
	signal(SIGINT, ft_signal);
	g_envs = ft_strs_to_envs(env);
	ft_manage_lvl();
	if (ac == 2)
		ft_putendl(av[1]);
	while (1)
	{
		ft_update_pwd();
		ft_get_prompt(&command);
		if (command && *command)
			ft_manage_execs(&command);
		else
			ft_putchar('\n');
	}
	return (EXIT_SUCCESS);
}
