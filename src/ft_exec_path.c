/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exec_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/19 14:54:34 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/30 15:26:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;

static char			**ft_get_tab_env(t_envs *envs)
{
	size_t		i;
	t_env		*env;
	char		**tab;

	env = envs->common;
	i = 0;
	while (env)
	{
		env = env->next;
		i++;
	}
	if (!(tab = ft_memalloc(sizeof(char*) * (i + 1))))
		ft_internal_error(0);
	env = envs->common;
	i = 0;
	while (env)
	{
		if (!(tab[i] = ft_strdup(env->var)))
			ft_internal_error(0);
		env = env->next;
		i++;
	}
	return (tab);
}

int					ft_exec_path(char *path, char **args, t_envs *envs)
{
	pid_t		pid;
	int			ret;
	char		**tab_envs;
	size_t		i;

	i = 0;
	if (access(path, X_OK) == 0)
	{
		pid = fork();
		if (pid == 0)
		{
			tab_envs = ft_get_tab_env(envs);
			ret = execve(path, args, tab_envs);
			exit(ret);
		}
		else
		{
			wait(&ret);
			g_id.last_exit = ret;
			return (EXIT_SUCCESS);
		}
	}
	else
		return (EXIT_FAILURE);
}
