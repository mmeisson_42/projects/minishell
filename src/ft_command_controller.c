/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_command_controller.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/07 13:51:03 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 13:07:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;

static char			**ft_get_tab_env(t_envs *envs)
{
	size_t		i;
	t_env		*env;
	char		**tab;

	env = envs->common;
	i = 0;
	while (env)
	{
		env = env->next;
		i++;
	}
	if (!(tab = ft_memalloc(sizeof(char*) * (i + 1))))
		ft_internal_error(0);
	env = envs->common;
	i = 0;
	while (env)
	{
		if (!(tab[i] = ft_strdup(env->var)))
			ft_internal_error(0);
		env = env->next;
		i++;
	}
	return (tab);
}

int					ft_manage_execution(char *path, char **args, t_envs *envs)
{
	int			ret;
	char		**tab_envs;
	size_t		i;

	i = 0;
	if (access(path, X_OK) == 0)
	{
		g_id.curr_id = fork();
		if (g_id.curr_id == 0)
		{
			tab_envs = ft_get_tab_env(envs);
			ret = execve(path, args, tab_envs);
			exit(ret);
		}
		else
		{
			wait(&ret);
			g_id.last_exit = ret;
			return (EXIT_SUCCESS);
		}
	}
	else
		return (EXIT_FAILURE);
}

char				**get_env_path(t_envs *envs, int common)
{
	size_t		i;
	t_env		*env;

	i = 0;
	if (common)
		env = envs->common;
	else
		env = envs->save;
	while (env)
	{
		if (!ft_strncmp(env->var, "PATH=", 5))
			return (ft_strsplit(env->var + 5, ':'));
		env = env->next;
	}
	return (NULL);
}

static int			ft_manage_exec_path(char **s_path, char **command,
		t_envs *envs)
{
	char			*path;
	int				i;

	i = 0;
	if (!(path = ft_str_rejoin(*s_path, *command)))
		ft_internal_error(0);
	if (ft_manage_execution(path, command, envs) == EXIT_SUCCESS)
	{
		while (s_path[i])
			free(s_path[i++]);
		return (EXIT_SUCCESS);
	}
	free(*s_path);
	return (EXIT_FAILURE);
}

int					ft_command_controller(char **command, t_envs *envs)
{
	char			**s_path;
	int				i;
	int				ft_bool;

	i = ft_direct_path(command, envs);
	if (i != 2)
		return (EXIT_SUCCESS);
	else
	{
		i = 0;
		ft_bool = 1;
		while (ft_bool-- >= 0)
			if ((s_path = get_env_path(envs, ft_bool)) != NULL)
			{
				while (s_path[i])
					if (ft_manage_exec_path(s_path + i++, command, envs)
							== EXIT_SUCCESS)
						return (ft_del_path(s_path));
				ft_del_path(s_path);
			}
	}
	g_id.last_exit = EXIT_FAILURE;
	ft_user_error(W_COMMAND);
	return (EXIT_FAILURE);
}
