/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minilib.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 11:47:15 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 13:08:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char			*ft_strreplace(char **str1, char *str2)
{
	if (*str1)
		free(*str1);
	if (!(*str1 = ft_strdup(str2)))
		ft_internal_error(0);
	return (*str1);
}

size_t			ft_double_strlen(char *str1, char *str2)
{
	size_t		size;

	size = 0;
	if (str1)
		size += ft_strlen(str1);
	if (str2)
		size += ft_strlen(str2);
	return (size);
}

char			*ft_str_rejoin(char *left, char *right)
{
	static char			*join = NULL;
	static size_t		join_size = 0;
	size_t				size;

	size = 0;
	size = ft_double_strlen(left, right);
	if (size >= join_size)
	{
		if (join)
			ft_strdel(&join);
		if (!(join = ft_memalloc(size + 2)))
			ft_internal_error(0);
		ft_strcpy(join, left);
		ft_strcat(join, "/");
		ft_strcat(join, right);
	}
	else
	{
		ft_strcpy(join, left);
		ft_strcat(join, right);
	}
	return (join);
}

int				ft_rewrite(char **var, char *value, size_t size, int rewrite)
{
	if (rewrite)
	{
		*var = ft_varcpy(*var, value, size);
		return (EXIT_SUCCESS);
	}
	return (EXIT_FAILURE);
}

int				ft_direct_path(char **command, t_envs *g_envs)
{
	int		ret;

	ret = 2;
	if (((*command)[0] == '.' && (*command)[1] == '/'))
	{
		ft_strcpy(*command, *command + 2);
		ret = ft_manage_execution(*command, command, g_envs);
	}
	else if ((*command)[0] == '/')
		ret = ft_manage_execution(*command, command, g_envs);
	if (ret == EXIT_FAILURE)
		return (ft_user_error(W_COMMAND));
	return (ret);
}
