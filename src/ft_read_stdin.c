/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_stdin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 10:21:40 by mmeisson          #+#    #+#             */
/*   Updated: 2016/01/27 14:56:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

ssize_t		ft_read_stdin(char **command)
{
	ssize_t		ret;
	char		*c;
	char		buffer[BUFF_SIZE + 1];

	*command = NULL;
	ft_bzero(buffer, BUFF_SIZE + 1);
	while ((ret = read(0, buffer, BUFF_SIZE)) > 0)
	{
		if (!(*command = ft_strover(*command, buffer)))
			ft_internal_error(0);
		if ((c = ft_strchr(*command, '\n')) ||
				(c = ft_strchr(*command, EOF)))
		{
			*c = '\0';
			break ;
		}
	}
	return (ret);
}
