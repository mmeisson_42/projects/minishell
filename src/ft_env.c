/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 10:38:53 by mmeisson          #+#    #+#             */
/*   Updated: 2016/03/31 20:11:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern t_id		g_id;
extern t_envs	*g_envs;

t_envs		*ft_load_sub_envs(t_env_opts *opts, t_envs *this_envs,
		char ***args)
{
	char		*c;
	t_envs		*sub_envs;
	char		*env;
	char		**str;

	str = *args;
	env = NULL;
	if (ft_is_option(opts, 'i'))
		sub_envs = ft_strs_to_envs(&env);
	else
		sub_envs = ft_envscpy(this_envs);
	while (*str)
	{
		if ((c = ft_strchr(*str, '=')))
		{
			*c = '\0';
			ft_envadd(*str, ++c, REWRITE, &(sub_envs->common));
		}
		else
			break ;
		str++;
	}
	*args = str;
	return (sub_envs);
}

void		ft_print_env(t_env *sub_env)
{
	while (sub_env)
	{
		ft_putendl(sub_env->var);
		sub_env = sub_env->next;
	}
}

int			ft_manage_exec(char **args, t_env_opts *opts, t_envs *sub_envs)
{
	char				**tmp;
	int					ret;

	tmp = args;
	if (ft_is_option(opts, 'P'))
		ret = ft_p_option(opts, args, sub_envs);
	else
		ret = ft_command_controller(args, sub_envs);
	ft_deloptsall(&opts);
	ft_delenvs(&sub_envs);
	return (ret);
}

int			ft_env(int ac, char **av)
{
	t_env_opts			*opts;
	char				**args;
	t_envs				*sub_envs;
	int					error;

	error = 0;
	g_id.last_exit = EXIT_FAILURE;
	if (ac < 1)
		return (ft_user_error(M_ARGS));
	args = NULL;
	sub_envs = ft_envscpy(g_envs);
	opts = ft_parse_options(av, &args, &sub_envs, &error);
	if (error == EXIT_FAILURE)
		return (EXIT_FAILURE);
	ft_u_option(opts, sub_envs);
	ft_verbose_env(opts, args);
	if (!args || !*args)
	{
		ft_print_env(sub_envs->common);
		ft_deloptsall(&opts);
		ft_delenvs(&sub_envs);
	}
	else
		return (ft_manage_exec(args, opts, sub_envs));
	return (EXIT_SUCCESS);
}
