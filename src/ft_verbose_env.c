/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_verbose_env.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 10:56:49 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/03 10:47:53 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void			level_one(t_env_opts *opts, char **args, int verb)
{
	int		i;

	i = 2;
	while (verb >= i)
	{
		ft_putstr("#env verbosity now at ");
		ft_putnbr(i++);
		ft_putchar('\n');
	}
	if (ft_is_option(opts, 'i'))
		ft_putendl("#env  clearing environ");
	if (args[0])
	{
		ft_putstr("#env executing:  ");
		ft_putendl(args[0]);
	}
	i = -1;
	while (args[++i])
	{
		ft_putstr("#env\targ[");
		ft_putnbr(i);
		ft_putstr("]= '");
		ft_putstr(args[i]);
		ft_putendl("'");
	}
}

static void			level_two(t_env_opts *opts)
{
	t_env_opts	*tmp_opts;

	tmp_opts = opts;
	while (tmp_opts)
	{
		if (tmp_opts->option == 'P')
		{
			ft_putstr("#env\tSearching: '");
			if (tmp_opts->arg)
				ft_putstr(tmp_opts->arg);
			ft_putendl("'");
		}
		tmp_opts = tmp_opts->next;
	}
	while (opts)
	{
		if (opts->option == 'u')
		{
			ft_putstr("#env\tunset: '");
			if (opts->arg)
				ft_putstr(opts->arg);
			ft_putendl("'");
		}
		opts = opts->next;
	}
}

void				ft_verbose_env(t_env_opts *opts, char **args)
{
	int		verb;

	verb = ft_is_option(opts, 'v');
	if (verb >= 1)
		level_one(opts, args, verb);
	if (verb >= 2)
		level_two(opts);
}
