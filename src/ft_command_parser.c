/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 10:10:29 by mmeisson          #+#    #+#             */
/*   Updated: 2016/02/19 15:14:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int				in_array(char *str, char c)
{
	while (*str)
	{
		if (*str == c)
			return (1);
		str++;
	}
	return (0);
}

static size_t	get_nb_arg(char *str)
{
	size_t		ret;
	static char	sep[4] = " \t\n";

	ret = 0;
	while (*str)
	{
		while (in_array(sep, *str))
			str++;
		if (*str)
			ret++;
		else
			break ;
		while (*str && !in_array(sep, *str))
			str++;
	}
	return (ret);
}

static char		**ft_strargsplit(char *str)
{
	size_t		nb_arg;
	char		**tab;
	static char	sep[4] = " \t\n";
	size_t		i;

	i = 0;
	nb_arg = get_nb_arg(str);
	if (!(tab = malloc(sizeof(char*) * (nb_arg + 1))))
		ft_internal_error(0);
	while (i < nb_arg)
	{
		while (in_array(sep, *str))
			str++;
		if (*str == '"' || *str == '\'')
			tab[i++] = ft_strdup_toc(&str, *str);
		else if (*str)
			tab[i++] = ft_strdup_tos(&str, sep);
	}
	tab[i] = NULL;
	return (tab);
}

char			**ft_command_parser(char **str, int *size)
{
	char	**tab;
	char	*tmp;
	size_t	i;

	i = 0;
	tab = NULL;
	if (*str)
	{
		tab = ft_strargsplit(*str);
		while (tab[i])
		{
			tmp = tab[i];
			tab[i] = ft_strtrim(tab[i]);
			ft_strdel(&tmp);
			i++;
		}
		*size = (int)i;
	}
	return (tab);
}
